/**
 * Created by mikebenn on 23/08/2016.
 */

// CONSTANTS

    var IDLE_TIME_SECS = 60;

var BUTTON_SELECTOR_CLASS = 'button',
    PATH_TO_SETUP_FILES = './setup/',
    PATH_TO_PAGES_JSON = PATH_TO_SETUP_FILES + 'pages.json',
    PATH_TO_LOGO = 'assets/logo-knowhow.png',
    PATH_TO_VIDEOS = 'videos/';


var PAGE_TYPE_HTML = 'html',
    PAGE_TYPE_VIDEO = 'video',
    PAGE_TYPE_CHECKLIST = 'checklist';

var isFirstRun = true;

//.................

$(document).ready(function(){
    pages.init();
});

var buttons = {
    backBtn: null,
    nextBtn: null,
    buttonStrip: null,
    init: function(){
        this.backBtn = $('#backBtn');
        this.nextBtn = $('#nextBtn');
        this.buttonStrip = $('.button_strip');
        this.addListeners();
    },
    addListeners: function(){
        $('.' + BUTTON_SELECTOR_CLASS).on('click', function(){
            if ($(this).hasClass('yes') || $(this).hasClass('no')) checklist.action($(this));
            else {
                var id = $(this).attr('data-action');
                if (typeof actions[id] === 'function') actions[id]($(this));
                else console.log('Not a function');
            }
        });
        $('#video0').click(function(){
            actions.next();
        });
        $('#startBtn').click(function(){
            if (isFirstRun){
                videos.play(0);
                videos.stop(0);
            }

            isFirstRun = false;

            actions.next();
        });
    },
    hideButtons: function(){
        this.buttonStrip.hide();
    },
    showButtons: function(){
        this.buttonStrip.show();
    }
};

//Each action corresponds to the data-action parameter of a button
//noinspection JSUnusedGlobalSymbols
var actions = {
    play: function(){
        console.log("Play");
    },
    back: function(){
        navigation.prevPage();
    },
    next: function(){
        navigation.nextPage();
    }
};

//noinspection JSUnusedGlobalSymbols
var checklist = {
    modalBG: null,
    modalHolder: null,
    modalContent: null,
    init: function(){
        this.modalBG = $('.modalBG');
        this.modalHolder = $('.modalHolder');
        this.modalContent = $('.modalContent');
    },
    action: function(el){
        el.addClass('selected');

        idleTimer.start();

        var parent = el.closest('.checklist_item'),
            parentPage = parent.closest('.page'),
            success = parentPage.find('.success');

        if (el.hasClass('yes')) {
            parent.find('.no').removeClass('selected');
            parent.find('.warning').hide();
            if (this.allBoxesTicked(el)) success.show();
        } else {
            parent.find('.yes').removeClass('selected');
            parent.find('.warning').css('display', 'flex');
            success.hide();
        }
    },
    allBoxesTicked: function(el){
        var flag = true;

        var parentPage = el.closest('.page');
        parentPage.find('.yes').each(function(){
            if (!$(this).hasClass('selected')) {
                flag = false;
                return false;
            }
        });
        return flag;
    },
    showModal: function(){
        var me = this;

        this.modalContent.html(pages.successHtml[navigation.currentPage])
            .append('<div id="modalNext" class="button centre">NEXT</div>');

        $('#modalNext').click(function(){
            me.hideModal();
        });

        this.modalBG.show();
        this.modalHolder.show();
    },
    hideModal: function(){
        this.modalBG.hide();
        this.modalHolder.hide();
        this.modalContent.html('');
        navigation.nextPage();
    },
    resetAll: function(){
        $('.yes').removeClass('selected');
        $('.no').removeClass('selected');
        $('.success').hide();
        $('.warning').hide();
    }
};

var pages = {
    successHtml: [],
    pagesDOM: null,
    init: function(){
        this.getJson();
        this.pagesDOM = $('#pages');
    },
    getJson: function(){
        var me = this;
        $.getJSON(PATH_TO_PAGES_JSON, function(data){
            me.generatePages(data);
        });
    },
    generatePages: function(data){
        for (var p in data){
            var page = data[p];
            switch (page.type){
                case PAGE_TYPE_HTML:
                    this.addHtmlPage(page, p);
                    break;
                case PAGE_TYPE_VIDEO:
                    this.addVideoPage(page, p);
                    break;
                case PAGE_TYPE_CHECKLIST:
                    this.addChecklistPage(page, p);
                    //this.successHtml[p] = page.success;
                    break
            }
        }

        this.pageLoadComplete();
    },
    pageLoadComplete: function(){
        buttons.init();
        navigation.init();
        checklist.init();
        videos.init();
    },
    addHtmlPage: function(page, index){
        var html = '<div id="page' + index + '" class="page">';
        html += page.content;
        html += '</div';

        this.pagesDOM.append(html);
    },
    addVideoPage: function(page, index){
        var path  = PATH_TO_VIDEOS + page.source;

        var loop = (index == 0)? ' loop':'';

        var html = '<div id="page' + index + '" class="page">';
        html += '<video id="video' + index + '"' + loop + '>';
        html += '<source src="' +
            path +
            '" type="video/mp4">';
        html += '</video>';
        html += '</div>';

        this.pagesDOM.append(html);

        videos.videoDOMs[index] = document.getElementById('video'+index);
    },
    addChecklistPage: function(page, index){
        var html = '<div id="page' + index + '" class="page">';
        html += '<img class="kh_logo" src="' + PATH_TO_LOGO + '" height="150">';

        html += '<h2>' + page.header + '</h2>';

        html += this.returnChecklistItems(page.items);

        html += '<div class="success">' +
            page.success +
            '</div>';

        html += '</div';

        this.pagesDOM.append(html);
    },
    returnChecklistItems: function(items){
        var html = '<table class="listHolder"><tr><td>';
        for (var i in items){
            var item = items[i],
                q = item['question'],
                w = item['warning'];

            html += '<div class="checklist_item">';
            html += '<p>' + q + '</p>';
            html += '<div class="yes button">YES</div><div class="no button">NO</div>';
            html += '<div class="warning">' + w + '</div>';
            html += '</div>';
        }
        html += '</td></tr></table>';
        return html;
    }
};

var navigation = {
    currentPage: null,
    numberOfPages: null,
    init: function(){
        this.numberOfPages = $('.page').length;
        this.showPage(1);
    },
    showPage: function(index){
        if (index == this.currentPage) return;

        //console.log('Show: ' + index);
        if (!isFirstRun){
            idleTimer.start();
        }

        if (index == 0 || index == 1) {
            buttons.hideButtons();
            checklist.resetAll();
        }
        else buttons.showButtons();

        videos.stop(this.currentPage);
        $('#page' + this.currentPage).hide();
        $('#page' + index).show();
        videos.play(index);


        this.currentPage = index;
    },
    nextPage: function(){
        var gotoPage = (this.currentPage < this.numberOfPages - 1)? this.currentPage + 1 : 0;
        this.showPage(gotoPage);
    },
    prevPage: function(){
        if (this.currentPage != 0){
            this.showPage(this.currentPage - 1);
        }
    }
};

var videos = {
    videoDOMs: [],
    init: function(){
        this.addEventListeners();
    },
    addEventListeners: function(){
        var me = this;
        for (var i in this.videoDOMs){
            this.videoDOMs[i].addEventListener('ended',me.ended,false);
        }
    },
    play: function(index){
        if (typeof this.videoDOMs[index] !== 'undefined'){
            this.videoDOMs[index].play();
        }
    },
    stop: function(index){
        if (typeof this.videoDOMs[index] !== 'undefined'){
            this.videoDOMs[index].pause();
            this.videoDOMs[index].currentTime = 0;
        }
    },
    ended: function(){
        navigation.nextPage();
    }
};

var idleTimer = {
    timer: null,
    start: function(){
        //console.log('T START');
        if (this.timer != null) clearTimeout(this.timer);
        this.timer = setTimeout(function(){
            //console.log('IDLE TIME OUT');
            navigation.showPage(0);
        }, IDLE_TIME_SECS * 1000);
    }
};